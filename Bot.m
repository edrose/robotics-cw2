classdef Bot < BotSim
    %BOT Wrapper class for controlling the NXT bot
    %   This is an immitation of BotSim that actually connects to the robot
    
    properties
        nxt % Reference to the NXT handle. Mostly unused
        l_motor % Reference to left motor
        r_motor % Reference to right motor
        s_motor % Reference to sensor motor
    end

    properties (Constant)
        ultrasonic = SENSOR_1; % Sensor port ultrasonic is connected to
        button = SENSOR_4; % Sensor port button is connected to
        circumference = 135; % Wheel circumference in mm
        move_offset = -3.7; % Fudge factor for circumference when moving
        turn_offset = -4.7385; % Fudge factor for circumference when turning
        wheel_base = 155 + 5; % Distance between wheels in mm + a bodge factor
        scan_power = 40; % Power for scanner motor
        scan_wait_time = 1; % Time to wait after moving before scanning
        scan_number_of_scans = 10; % Number of scans to perform
        turn_speed = 40; % Speed in percent that a turn is performed at
        move_speed = 50; % Speed in percent that a move is performed at
    end
    
    methods
        function obj = Bot()
            %BOT Open a connection to the NXT Brick and set the motors up            
            obj@BotSim([0,0; 110,0; 110,110; 44,110; 44,66; 66,66; 66,44; 0,44]);

            % Send open the NXT connection and set it as default
            obj.nxt = COM_OpenNXT();
            COM_SetDefaultNXT(obj.nxt);
            
            % Get references to all of the motors
            obj.l_motor = NXTMotor('C');
            obj.r_motor = NXTMotor('A');
            obj.s_motor = NXTMotor('B');
            
            % Hold sensor when not moving
            obj.s_motor.ActionAtTachoLimit = 'HoldBrake';

            % Open the ultrasonic and button sensor
            OpenUltrasonic(obj.ultrasonic);       
            OpenSwitch(obj.button);

        end
        
        function [dist, na] = ultraScan(obj)
            %ULTRASCAN Perform a full scan of the surroundings
            %  Run a scan taking the number of scans defined in 
            %    scan_number_of_scans. Assumes the scanner starts facing
            %    forwards or slightly clockwise from forward

            % Return the motor to the front
            if GetSwitch(obj.button) == 0
                calibrate_clockwise();
            end
            
            res = 360/obj.scan_number_of_scans;

            % Assume that the robot starts counter clockwise, ie a negative
            % motor power
            
            % Go anti-clockwise first
            obj.s_motor.Power = -1 *obj.scan_power;
            dist = ones(obj.scan_number_of_scans, 1) * 255;
            for i = obj.scan_number_of_scans:-1:obj.scan_number_of_scans/2 + 1
                obj.s_motor.TachoLimit = res;
                obj.s_motor.SendToNXT();
                obj.s_motor.WaitFor();
                pause(0.2)
                dist(i) = GetUltrasonic(obj.ultrasonic);
            end
            
            % Now go clockwise
            calibrate_clockwise();
            obj.s_motor.Power = obj.scan_power;
            dist(1) = GetUltrasonic(obj.ultrasonic);
            for i = 2: obj.scan_number_of_scans/2
                obj.s_motor.TachoLimit = res;
                obj.s_motor.SendToNXT();
                obj.s_motor.WaitFor();
                pause(0.2)
                dist(i) = GetUltrasonic(obj.ultrasonic);           
            end
            dist = flipud(dist);
            calibrate_counter_clockwise();

            function calibrate_clockwise()
                %CALIBRATE_CLOCKWISE Return sensor to forward position
                %   Function used to calibrate the Ultra Sound 
                %   Sensor rotation motor. Turn to the clockwise until the
                %   push sensor signal is register. This assmes that the sensor
                %   starts on the right of the cam 
                while(GetSwitch(obj.button) == 0)
                    obj.s_motor.Power = +15;
                    obj.s_motor.TachoLimit = 0;
                    obj.s_motor.SendToNXT();
                end
                obj.s_motor.Power = 0;
                obj.s_motor.SendToNXT();
                pause(0.5)
                na = NaN;
            end
        
            function calibrate_counter_clockwise()
                %CALIBRATE_COUNTER_CLOCKWISE Return sensor to forward posiiton
                %   Function used to calibrate the Ultra Sound 
                %   Sensor rotation motor. Turn to the clockwise until the
                %   push sensor signal is register. This assmes that the sensor
                %   starts on the right of the cam 
                while(GetSwitch(obj.button) == 0)
                    obj.s_motor.Power = -15;
                    obj.s_motor.TachoLimit = 0;
                    obj.s_motor.SendToNXT();
                end
                obj.s_motor.Power = 0;
                obj.s_motor.SendToNXT();
                pause(0.5)
            end
        end
        
        function turn(obj, t)
            % TURN turn the robot a given number of degrees on the spot
            % The basic idea here is to move each wheel a distance that is
            % equivalent to a t/360 of the diameter of the wheel base. 

            % Calculate the distance each wheel needs to turn for the given angle
            dist = -1 * pi * obj.wheel_base * t/(2*pi);

            % Move the motors that distance, making the right motor the inverse
            if dist > 1
                obj.l_motor.Power = obj.turn_speed;
                obj.r_motor.Power = -obj.turn_speed;
            else
                obj.l_motor.Power = -obj.turn_speed;
                obj.r_motor.Power = obj.turn_speed;
            end
            obj.l_motor.TachoLimit = abs(round(dist/(obj.circumference + obj.turn_offset) * 360));
            obj.r_motor.TachoLimit = abs(round(dist/(obj.circumference + obj.turn_offset) * 360));

            % Initiate the motor turns
            obj.l_motor.SendToNXT();
            obj.r_motor.SendToNXT();
            
            %wait for the motor to stop turning
            obj.l_motor.WaitFor();
            obj.r_motor.WaitFor();
        end
        
        function move(obj, dist)
            % MOVE Move the robot a given distance in a straight line
            % Move the robot a number of centimetres, returns the distance
            % travelled. If the robot hits an obstacle then the returned
            % distance will be the distance the robot travelled before
            % hitting the obstacle.
            
            % Get the required rotation in degrees
            t = dist/(obj.circumference + obj.move_offset) * 360 * 10;

            % Set each motors turn limit
            obj.l_motor.TachoLimit = round(abs(t));
            obj.r_motor.TachoLimit = round(abs(t));
            
            % Set each motor speed
            if t > 0
                obj.l_motor.Power = obj.move_speed;
                obj.r_motor.Power = obj.move_speed;
            else
                obj.l_motor.Power = -obj.move_speed;
                obj.r_motor.Power = -obj.move_speed;
            end

            % Reset current motor counters
            obj.l_motor.ResetPosition();
            obj.r_motor.ResetPosition();
            
            % Send to motor
            obj.l_motor.SendToNXT();
            obj.r_motor.SendToNXT();
            
            %wait for the motor to stop turning
            obj.l_motor.WaitFor();
            obj.r_motor.WaitFor();
        end
        
        function close(obj)
            % Close the connection to the NXT robot
            obj.s_motor.ActionAtTachoLimit = 'Coast';
            obj.s_motor.Power = 0;
            obj.s_motor.SendToNXT();
            COM_CloseNXT(obj.nxt);
        end
        
    end
end

