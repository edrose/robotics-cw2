%This function returns botSim, and accepts, botSim, a map and a target.
%LOCALISE Template localisation function
%function [botSim] = charles_localise(botSim,map,target)
%Setting up  the map, clearing variable
function [botSim] = localise(robot,map,target)
% clear all
% clf;
% close all;
NUMBER_OF_SCANS = 10;
variance = 0.5;
% map=[0,0;60,0;60,45;45,45;45,59;106,59;106,105;0,105];
% map  = [0,40;0,100;40,100;40,80;70,80;70,110;50,110;50,140;110,140;110,110;90,110;90,80;120,80;120,105;160,105;160,35;120,35;120,60;90,60;90,30;110,30;110,0;50,0;50,30;70,30;70,60;40,60;40,40];
%  map = [0,0;0,100;200,100;200,0;170,30;180,30;180,40;165,40;140,65;150,65;140,80;60,80;50,65;60,65;35,40;20,40;20,30;30,30];
sensor_noise = 0;
movement_noise = 0.011 * 1.1;
turning_nosie = 0.006 * 1.1;
% 
%robot = BotSim(map,[sensor_noise,movement_noise,turning_nosie]);
%robot = BotSim(map);
%robot.randomPose(10)

%% setup code
%you can modify the map to take account of your robots configuration space
modifiedMap = map; %you need to do this modification yourself
% robot.setMap(modifiedMap);
 robot.setScanConfig(robot.generateScanConfig(NUMBER_OF_SCANS));
%generate some random particles inside the map
num =800; % number of particles
particles(num,1) = BotSim; %how to set up a vector of objects
for i = 1:num
    particles(i) = BotSim(modifiedMap,[sensor_noise,movement_noise,turning_nosie]);  %each particle should use the same map as the botSim object
    particles(i).randomPose(5); %spawn the particles in random locations
    %particles(i).setScanConfig(generateScanConfig(particles(i), scans));
    particles(i).setScanConfig(particles(i).generateScanConfig(NUMBER_OF_SCANS))
end

%% Localisation code
maxNumOfIterations = 40;
n = 0;
converged =0; %The filter has not converged yet
weights = ones(num,1);
weights = weights/num; %normalising weights

while(converged == 0 && n < maxNumOfIterations) %%particle filter loop
    weights = ones(num,1);
    weights = weights/num; %normalising weights
    n = n+1; %increment the current number of iterations
    %sensor update
    botScan = robot.ultraScan(); %get a scan from the real robot.
    %find potential robot angle
    [sortScan, scanPos]  = sort(botScan);
    score = zeros(num, NUMBER_OF_SCANS);

    %% Write code for updating your particles scans
    for i=1:num
        [particleScan, ~] = particles(i).ultraScan();
        %         error_mat =  particleScan - botScan;
        %         error_mat = error_mat.^2;
        %         %MSE
        %         error_var = var(error_mat);
        %         error_mean = mean(error_mat);
        %         %error_squared_mean = error_mean.^2;
        %         %weights(i) = sigmf(error_mean, [0,1]);
        %         weights(i) =
        
        %% Write code for scoring your particles
        
        temp_scans = particleScan;
        botScan2 = botScan;
        
        
        for j = 1:NUMBER_OF_SCANS
            idx_limit  = [botScan == 255];
            botScan2  = botScan;
            botScan2(idx_limit)  = temp_scans(idx_limit);
            score(i, j) = 1/sqrt(sum((temp_scans - botScan2).^2));
            %score(i, j) = sum((temp_weights - botScan).^2);
            temp_scans = circshift(particleScan, j);
            %keep the best score 
        end
         [weights(i),angle_turn] = max(score(i,:));
         particles(i).turn(((angle_turn-1)*2*pi)/NUMBER_OF_SCANS);
    end
    weights + 0.1;
    normalizing_factor = sum(weights);
    weights  = weights/normalizing_factor;
    [~, pos] = sort(weights);
    particles_new(num,1) = BotSim;
    for x = 1:num
        particles_new(x) = BotSim(modifiedMap,[sensor_noise,movement_noise,turning_nosie]) ;  %each particle should use the same map as the botSim object
        particles_new(x).randomPose(0);
        particles_new(x).setScanConfig(particles_new(x).generateScanConfig(NUMBER_OF_SCANS));
    end
    %% Write code for resampling your particles
    beta = 0;
    max_weight = max(weights);
    index = 1;

    for k = 1:num
        beta = beta + rand() *2.0*max_weight;
        while beta >= (weights(index))
           beta = beta - weights(index);
           index = mod(index, num)+1;
        end
        particles_new(k).setBotPos(particles(index).getBotPos() + (-2 + (2+2)*rand(1,2)));
        particles_new(k).setBotAng(particles(index).getBotAng());
       
    end
    
    %% Write code to check for convergence
      particles = particles_new;
    
    %% Write code to decide how to move next
    % here they just turn in cicles as an example
    vals = vertcat(botScan(1:3), botScan(8:10));
    [~, maxI] = max(vals);
    if maxI > 3
        maxI = maxI + 4;
    end

    %[~, maxI] = max(botScan[]);
    turn = 2*pi/NUMBER_OF_SCANS * (maxI-1);
    if turn > pi
        turn = -1 * (2*pi-turn);
    end
    
    if min(vertcat(botScan(1:2), botScan(10))) <= 10
        move = -10;
        turn = 0;
    else
        move = min([7, botScan(maxI)]);
    end
    robot.turn(turn); %turn the real robot.
    robot.move(move); %move the real robot. These movements are recorded for marking
    for i =1:num %for all the particles.
        particles(i).turn(turn); %turn the particle in the same way as the real robot
        particles(i).move(move); %move the particle in the same way as the real robot
    end
    
    %% Drawing:
    %only draw if you are in debug mode or it will be slow during marking
    if robot.debug()
        hold off; %the drawMap() function will clear the drawing when hold is off
        robot.drawMap(); %drawMap() turns hold back on again, so you can draw the bots
        robot.drawBot(30,'g'); %draw robot with line length 30 and green
        for i =1:num
            particles(i).drawBot(3); %draw particle with line length 3 and default color
        end
        drawnow;
    end
    [ophion,centroids] = converges(robot,map,num,particles,50);
    condition = [5,5]; 
    [ontos,logos,done,cent_part] = converges_2(centroids,10,condition);
    ontos = sqrt(ontos);
    pneuma = cent_part(cent_part(:,1) < (ontos(1)+logos(1)) & cent_part(:,1) > (ontos(1)-logos(1))& cent_part(:,2) < (ontos(2)+logos(2)) & cent_part(:,2) > (ontos(2)-logos(2)) ,:);
    mean_cluster = mean(pneuma);
    sd_cluster  = var(pneuma); 
    if sd_cluster <= condition
        hold off;
       converged = 1;
       guess(1) = BotSim(map);
       guess(1).setBotPos(mean_cluster);
       [best_weights1,partIdx1] = max(score(:,1));
       angle = converges_angle(robot,map,num,particles,50);
       guess(1).setBotAng(particles(partIdx1).getBotAng());
       botSim = guess;
       guess(2) = BotSim(map);
       guess(2).setBotPos(mean_cluster);
       [best_weights,partIdx] = max(weights);
       angle = converges_angle(robot,map,num,particles,50);
       guess(2).setBotAng(particles(partIdx).getBotAng());
       arrayfun(@(x) x.delete(),particles,'un',0);
       robot.drawMap(); %drawMap() turns hold back on again, so you can draw the bots
       robot.drawBot(30,'k');
       guess(1).drawBot(15,'b');
       %guess(2).drawBot(15,'r');
    end
end
end
%end

function [angle]  =  converges_angle(robot,map,num,particles,numMeans)
    B = arrayfun(@(x) x.getBotAng(),particles,'un',0);
    B = cell2mat(B);
    r = randi([1 num],1,numMeans);
    C = B(r,:);
    C = [C,C];
    n = 0;
    centroids = zeros(numMeans,1);
    for i = 1:length(C)
        estimate = C(i,1:2);
        converged = 0;
        while converged == 0
            centroid_old = centroids(i,:);
            K = B-estimate;
            K = K.^2;
            K = sum(K,2);
            K = exp(-K);
            Q = K.*B;
            centroids(i,:) = sum(Q,1)/sum(K);
            
            if centroids(i,:) == centroid_old
               converged = 1;
            end
        end
        
    end
        angle = mean(centroids);
end


function [n,centroids] = converges(robot,map,num,particles,numMeans)
    B = arrayfun(@(x) x.getBotPos(),particles,'un',0);
    B = cell2mat(B);
    r = randi([1 num],1,numMeans);
    C = B(r,:);
    C = [C,C];
    n = 0;
    centroids = zeros(numMeans,2);
    for i = 1:length(C)
        estimate = C(i,1:2);
        converged = 0;
        while converged == 0
            centroid_old = centroids(i,:);
            K = B-estimate;
            K = K.^2;
            K = sum(K,2);
            K = exp(-K);
            Q = K.*B;
            centroids(i,:) = sum(Q,1)/sum(K);
            
            if centroids(i,:) == centroid_old
               converged = 1;
            end
        end
        
    end
end

function [var_cluster,mean_cluster,done,centroids] = converges_2(centroids_particles,numMeans,condition)
    B = centroids_particles;
    r = randi([1 length(centroids_particles)],1,numMeans);
    C = B(r,:);
    done = 0;
    centroids = zeros(numMeans,2);
    for i = 1:length(C)
        estimate = C(i,1:2);
        converged = 0;
        while converged == 0
            centroid_old = centroids(i,:);
            K = B-estimate;
            K = K.^2;
            K = sum(K,2);
            K = exp(-K);
            Q = K.*B;
            centroids(i,:) = sum(Q,1)/sum(K);
            
            if centroids(i,:) == centroid_old
               converged = 1;
            end
        end
    end
    mean_cluster = mean(centroids);
    var_cluster  = var(centroids); 
    if var_cluster <= condition
       done = 0; 
    end
end